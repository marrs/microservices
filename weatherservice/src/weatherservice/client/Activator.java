package weatherservice.client;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.BundleContext;

import weatherservice.api.WeatherService;

public class Activator extends DependencyActivatorBase {
	@Override @SuppressWarnings("serial")
	public void init(BundleContext bc, DependencyManager dm) throws Exception {
		dm.add(createComponent()
			.setInterface(Object.class.getName(), new Properties() {{
				put(CommandProcessor.COMMAND_SCOPE, "weather");
				put(CommandProcessor.COMMAND_FUNCTION, new String[] { "city", "cities" });
				}})
			.setImplementation(Client.class)
			.add(createServiceDependency()
				.setService(WeatherService.class)
				.setRequired(false))
		);
	}

	@Override
	public void destroy(BundleContext bc, DependencyManager dm) throws Exception {}
}
