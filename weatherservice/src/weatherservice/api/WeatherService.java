package weatherservice.api;

/** Service that predicts the weather. */
public interface WeatherService {
	/** Predicts the temperature in a specific city. */
	double predictTemperature(String city);
}
