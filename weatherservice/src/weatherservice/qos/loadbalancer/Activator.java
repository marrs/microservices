package weatherservice.qos.loadbalancer;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;

import weatherservice.api.WeatherService;
import weatherservice.qos.QualityOfService;

public class Activator extends DependencyActivatorBase {
	@Override @SuppressWarnings("serial")
	public void init(BundleContext bc, DependencyManager dm) throws Exception {
		dm.add(createComponent()
			.setInterface(WeatherService.class.getName(), new Properties() {{
				put(Constants.SERVICE_RANKING, 100); }})
			.setImplementation(LoadBalancer.class)
			.add(createServiceDependency()
				.setService(QualityOfService.class)
				.setRequired(false)
				.setCallbacks("add", "remove"))
		);
	}

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {}
}
