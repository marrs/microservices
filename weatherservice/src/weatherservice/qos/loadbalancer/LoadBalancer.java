package weatherservice.qos.loadbalancer;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import weatherservice.api.WeatherService;
import weatherservice.qos.QualityOfService;

public class LoadBalancer implements WeatherService {
	private CopyOnWriteArrayList<QualityOfService> m_qualities = new CopyOnWriteArrayList<>();
	private volatile BundleContext m_context;

	public void add(QualityOfService service) {
		m_qualities.add(service);
	}

	public void remove(QualityOfService service) {
		m_qualities.remove(service);
	}

	@Override
	public double predictTemperature(String city) {
		CopyOnWriteArrayList<QualityOfService> qualities = m_qualities;
		double random = Math.random() * totalWeight(qualities);
		for (QualityOfService qos : qualities) {
			double weight = qos.weight();
			if (random < weight) {
				WeatherService service = lookup(qos.serviceId());
				try {
					return service.predictTemperature(city);
				}
				catch (Exception e) {
					return 0;
				}
			}
			else {
				random -= weight;
			}
		}
		return 0;
	}
	
	double totalWeight(List<QualityOfService> list) {
		return list.stream().mapToDouble(QualityOfService::weight).sum();
	}
	
	WeatherService lookup(long id) {
		ServiceReference[] sr;
		try {
			sr = m_context.getServiceReferences(WeatherService.class.getName(), "(|(service.id=" + id + ")(org.apache.felix.dependencymanager.aspect=" + id + "))");
			if (sr != null && sr.length > 0) {
				Arrays.sort(sr);
				WeatherService service = (WeatherService) m_context.getService(sr[sr.length - 1]);
				return service;
			}
		}
		catch (NullPointerException | InvalidSyntaxException e) {}
		return null;
	}
}
