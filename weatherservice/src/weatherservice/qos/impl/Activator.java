package weatherservice.qos.impl;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.BundleContext;

import weatherservice.api.WeatherService;
import weatherservice.qos.QualityOfService;

public class Activator extends DependencyActivatorBase {
	@Override @SuppressWarnings("serial")
	public void init(BundleContext bc, DependencyManager dm) throws Exception {
		dm.add(createComponent()
			.setInterface(Object.class.getName(), new Properties() {{
				put(CommandProcessor.COMMAND_SCOPE, "qos");
				put(CommandProcessor.COMMAND_FUNCTION, new String[] { "average" });
				}})
			.setImplementation(QualityOfServiceCommand.class)
			.add(createServiceDependency()
				.setService(QualityOfService.class)
				.setRequired(false)
				.setCallbacks("add", "remove"))
		);
		dm.add(createAspectService(WeatherService.class, "(service.imported=true)", 50)
			.setInterface(QualityOfService.class.getName(), null)
			.setImplementation(QualityOfServiceImpl.class)
		);
	}

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {}
}
