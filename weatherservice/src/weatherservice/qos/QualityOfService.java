package weatherservice.qos;

public interface QualityOfService {
	public String serviceName();
	public long serviceId();
	public long averageInvocationTime();
	public double weight();
}
