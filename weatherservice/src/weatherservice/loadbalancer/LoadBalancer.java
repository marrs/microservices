package weatherservice.loadbalancer;

import java.util.concurrent.CopyOnWriteArrayList;

import weatherservice.api.WeatherService;

public class LoadBalancer implements WeatherService {
	private volatile int m_index = 0;
	private CopyOnWriteArrayList<WeatherService> m_services = new CopyOnWriteArrayList<>();

	public void add(WeatherService service) {
		m_services.add(service);
	}

	public void remove(WeatherService service) {
		m_services.remove(service);
	}

	@Override
	public double predictTemperature(String city) {
		CopyOnWriteArrayList<WeatherService> services = m_services;
		int attempts = 5;
		while (attempts-- > 0) {
			if (services.size() == 0) {
				return 0;
			}
			if (m_index >= services.size()) {
				m_index = 0;
			}
			WeatherService service = services.get(m_index++);
			try {
				return service.predictTemperature(city);
			}
			catch (RuntimeException e) {
				// whenever we get a runtime exception, we retry a number of times
			}
		}
		return 0;
	}
}
