package weatherservice.node.client;

import java.util.concurrent.CopyOnWriteArrayList;

import weatherservice.node.Node;


public class Client {
	private CopyOnWriteArrayList<Node> m_nodes = new CopyOnWriteArrayList<>();

	public void add(Node node) {
		m_nodes.add(node);
	}

	public void remove(Node node) {
		m_nodes.remove(node);
	}

	public void check() {
		System.out.println("Checking nodes:");
		for (Node node : m_nodes) {
			try {
				System.out.println("" + node.name()  + ": " + (node.healthCheck() ? "healthy" : "not healthy"));
			}
			catch (Exception e) {
				System.out.println("remote node: call failed");
			}
		}
	}
}
