package weatherservice.node.client;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.BundleContext;

import weatherservice.node.Node;

public class Activator extends DependencyActivatorBase {
	@Override @SuppressWarnings("serial")
	public void init(BundleContext bc, DependencyManager dm) throws Exception {
		dm.add(createComponent()
			.setInterface(Object.class.getName(), new Properties() {{
				put(CommandProcessor.COMMAND_SCOPE, "node");
				put(CommandProcessor.COMMAND_FUNCTION, new String[] { "check" });
				}})
			.setImplementation(Client.class)
			.add(createServiceDependency()
				.setService(Node.class)
				.setRequired(false)
				.setCallbacks("add", "remove"))
		);
	}

	@Override
	public void destroy(BundleContext bc, DependencyManager dm) throws Exception {}
}
