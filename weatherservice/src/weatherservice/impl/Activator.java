package weatherservice.impl;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

import weatherservice.api.WeatherService;

public class Activator extends DependencyActivatorBase {
	@Override @SuppressWarnings("serial")
	public void init(BundleContext bc, DependencyManager dm) throws Exception {
		dm.add(createComponent()
			.setInterface(WeatherService.class.getName(), new Properties() {{ 
				put(RemoteConstants.SERVICE_EXPORTED_INTERFACES, WeatherService.class.getName()); }})
			.setImplementation(WeatherServiceImpl.class)
		);
	}

	@Override
	public void destroy(BundleContext bc, DependencyManager dm) throws Exception {}
}
