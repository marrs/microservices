package launcher.bootstrap;

import java.net.ServerSocket;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Bootstrap implements BundleActivator {
	@Override
	public void start(final BundleContext context) throws Exception {
		ServerSocket s = new ServerSocket(0);
		int port = s.getLocalPort();
		System.out.println("bound to port: " + port);
		System.setProperty("org.osgi.service.http.port", "" + port);
		s.close();
	}

	@Override
	public void stop(BundleContext context) throws Exception {}
}
